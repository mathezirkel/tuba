import sbt._

object Dependencies {

  // Unit and integration tests
  val scalatest = "org.scalatest" %% "scalatest" % "3.2.2"

  // Property based testing
  val scalacheck = "org.scalacheck" %% "scalacheck" % "1.15.1"

  // Use NOP logger for slf4j to disable logging during testing
  val `slf4j-nop` = "org.slf4j" % "slf4j-nop" % "1.7.30"

  // Postgres JDBC settings
  val postgresql = "org.postgresql" % "postgresql" % "42.2.18"

  // Quill for database handling
  val quill = "io.getquill" %% "quill-jdbc" % "3.5.3"

}
