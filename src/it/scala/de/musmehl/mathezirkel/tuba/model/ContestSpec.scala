package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class ContestSpec extends DatabaseTestBase {
  "Contest object" should "create, retrieve, update, and destroy a Contest object" in {
    val c        = Contest.create(name = "117. Mathe-Olympiade")
    val Some(c1) = Contest.retrieve(c.id)
    assert(c === c1)

    val NEW_NAME = "118. Mathe-Olympiade"

    var affectedRows = Contest.update(c.copy(name = NEW_NAME))
    val Some(c2)     = Contest.retrieve(c.id)
    assert(affectedRows === 1)
    assert(c2.name === NEW_NAME)

    affectedRows = Contest.delete(c2)
    val c3 = Contest.retrieve(c.id)
    assert(affectedRows === 1)
    assert(c3 === None)
  }
}
