package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.DatabaseTestBase

class SchoolSpec extends DatabaseTestBase {
  "School object" should "create, retrieve, update, and destroy a School object" in {
    val s = School.create(
      name = "Rudolf-Diesel-Gymnasium",
      schoolType = SchoolType.Gymnasium,
      addressStreet = Some("Peterhofstraße"),
      addressHousenumber = Some("9"),
      addressPostcode = Some("86163"),
      addressPlace = Some("Augsburg"),
      officialId = None,
      numberMathTeachers = Some(20)
    )
    val Some(s1) = School.retrieve(s.id)
    assert(s === s1)

    val OFFICIAL_ID = "815482542"

    var affectedRows = School.update(s.copy(officialId = Some(OFFICIAL_ID)))
    val Some(s2)     = School.retrieve(s.id)
    assert(affectedRows === 1)
    assert(s2.officialId === Some(OFFICIAL_ID))

    affectedRows = School.delete(s2)
    val s3 = School.retrieve(s.id)
    assert(affectedRows === 1)
    assert(s3 === None)
  }
}
