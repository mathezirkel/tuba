/* Copyright (C) 2021 Sven Prüfer - All Rights Reserved
 *
 * This file is part of Tuba.
 *
 * Tuba is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tuba is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tuba.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Default content of Tuba database.
 *
 * This script assumes a current Tuba schema under `mathezirkel_db`.
 * Executing this script will add default entries to the database which
 * are not part of the schema but which are expected to be used in any
 * operational setting.
 */

-- Schema
SET SCHEMA 'mathezirkel_db';

-- Illness
INSERT INTO illness (name) VALUES ('Asthma');
INSERT INTO illness (name) VALUES ('Gräserallergie');
INSERT INTO illness (name) VALUES ('Epilepsie');
INSERT INTO illness (name) VALUES ('Pollenallergie');
INSERT INTO illness (name) VALUES ('Skoliose');
INSERT INTO illness (name) VALUES ('Milbenallergie');
INSERT INTO illness (name) VALUES ('Hausstauballergie');
INSERT INTO illness (name) VALUES ('Sonnenempfindlichkeit');
INSERT INTO illness (name) VALUES ('Heuschnupfen');
INSERT INTO illness (name) VALUES ('Aufmerksamkeitsdefizit-/Hyperaktivitätsstörung');
INSERT INTO illness (name) VALUES ('Migräne');
INSERT INTO illness (name) VALUES ('Autismus');
INSERT INTO illness (name) VALUES ('Bienenallergie');
INSERT INTO illness (name) VALUES ('Wespenallergie');
INSERT INTO illness (name) VALUES ('Schlafwandeln');
INSERT INTO illness (name) VALUES ('Pferdeallergie');
