package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** A participant for a [[Contest]] */
case class ContestParticipant(contest: Contest.ID, person: Person.ID)

object ContestParticipant {

  /** Return the query which contains all [[ContestParticipant ContestParticipants]] */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote {
      querySchema[ContestParticipant]("mathezirkel_db.contest_participant")
    }
  }

  /** Retrieve the list of [[ContestParticipant ContestParticipants]] for a given [[Person]] */
  def forPersonID(
      id: Person.ID
  )(implicit ctx: PostgresContext): List[ContestParticipant] = {
    import ctx._
    ctx.run(all.filter(_.person == lift(id)))
  }

  /** Associate a [[Contest]] to a [[Person]]. */
  def create(
      contest: Contest.ID,
      person: Person.ID
  )(implicit ctx: PostgresContext): ContestParticipant = {
    import ctx._
    val contestParticipant = ContestParticipant(contest, person)
    ctx.run(all.insert(lift(contestParticipant)))
    contestParticipant
  }

  /** Delete a [[ContestParticipant]] in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(
      contestParticipant: ContestParticipant
  )(implicit ctx: PostgresContext): Long = {
    import ctx._
    ctx.run(
      all
        .filter(participant =>
          participant.person == lift(
            contestParticipant.person
          ) && participant.contest == lift(
            contestParticipant.contest
          )
        )
        .delete
    )
  }
}
