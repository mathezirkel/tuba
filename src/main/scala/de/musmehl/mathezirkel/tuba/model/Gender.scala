package de.musmehl.mathezirkel.tuba.model

import io.getquill._

sealed trait Gender { val id: Int; val name: String }

object Gender {
  case object Female extends Gender { val id = 1; val name = "Female" }
  case object Male   extends Gender { val id = 2; val name = "Male"   }

  implicit val encodeGender = MappedEncoding[Gender, Int](_.id)
  implicit val decodeGender = MappedEncoding[Int, Gender] {
    case Female.id => Female
    case Male.id   => Male
  }
}
