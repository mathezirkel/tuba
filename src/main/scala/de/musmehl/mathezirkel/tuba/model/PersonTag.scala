package de.musmehl.mathezirkel.tuba.model

import de.musmehl.mathezirkel.tuba.PostgresContext

/** A person tag associates additional information with a person */
private[tuba] case class PersonTag(
    personId: Person.ID,
    key: String,
    value: String
)

private[tuba] object PersonTag {

  /** Return the query which contains all person tags */
  def all(implicit ctx: PostgresContext) = {
    import ctx._
    quote { querySchema[PersonTag]("mathezirkel_db.person_tag") }
  }

  /** Retrieve the person tags for a given person */
  def forPersonID(
      id: Person.ID
  )(implicit ctx: PostgresContext): Map[String, String] = {
    import ctx._
    ctx
      .run(all.filter(_.personId == lift(id)))
      .map(tag => (tag.key, tag.value))
      .toMap
  }

  /** Create a tag for a Person. */
  def create(personId: Person.ID, key: String, value: String)(implicit
      ctx: PostgresContext
  ): PersonTag = {
    import ctx._
    val tag = PersonTag(personId, key, value)
    ctx.run(all.insert(lift(tag)))
    tag
  }

  /** Update a tag for a Person. */
  def update(personId: Person.ID, key: String, value: String)(implicit
      ctx: PostgresContext
  ): Long = {
    import ctx._
    val personTag = PersonTag(personId, key, value)
    ctx.run(
      all
        .filter(tag => tag.personId == lift(personId) && tag.key == lift(key))
        .update(lift(personTag))
    )
  }

  /** Delete a person tag in the database
    *
    *  @return the number of affected rows (1 if successful)
    */
  def delete(personId: Person.ID, key: String)(implicit
      ctx: PostgresContext
  ): Long = {
    import ctx._
    ctx.run(
      all
        .filter(tag => tag.personId == lift(personId) && tag.key == lift(key))
        .delete
    )
  }
}
