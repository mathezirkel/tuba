package de.musmehl.mathezirkel

import io.getquill._

/** =Tuba=
  *
  *  Library for accessing the Mathezirkel Database.
  *
  *  ==Usage==
  *  TBC
  */
package object tuba {

  /** The context type of the Tuba database: Postgresql database with SnakeCase naming convention */
  type PostgresContext = PostgresJdbcContext[SnakeCase.type]
}
